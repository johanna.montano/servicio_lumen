<?php

namespace App\Http\Controllers;

use App\Mensaje;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;



class MensajeController extends BaseController
{

#Listar todas los mensajes
    public function index(Request $request){
   		$mensajes = Mensaje::all();
   	  return response()->json($mensajes, 200);
	}

#Crear un mensaje
	public function createMensaje(Request $request){
   	$mensajes = new Mensaje();
   	$mensajes->mensaje = $request->mensaje;
   	$mensajes->publicacion_id = $request->publicacion_id;
  	$mensajes->mascota_id = $request->mascota_id;
   	$mensajes->save();
	}

#Listar mensajes por mascotas
  public function getMensaje(Request $request, $id){
    $mensajes = Mensaje::where('mascota_id', $id)->get();
    return response()->json($mensajes, 200);      
  }

  #Eliminar mensaje
   public function deletePostulacion(Request $request, $id){
    $mensajes = Mensaje::where('id', $id)->first();
    $mensajes->delete();          
   }     

}
