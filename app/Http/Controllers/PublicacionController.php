<?php

namespace App\Http\Controllers;

use App\Publicacion;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;


class PublicacionController extends BaseController
{
#Listar todos las publicaciones
    public function index(Request $request){
   		$publicaciones = Publicacion::all();
   		return response()->json($publicaciones, 200);
	}
#Crear Publicacion
	/*public function createPublicacion(Request $request){
		$publicacion = new Publicacion();
		$publicacion->titulo = $request->titulo;
		$publicacion->descripcion = $request->descripcion;
		$publicacion->mascota_id = $request->mascota_id;
		$publicacion->save();
	}*/

	public function createPublicacion(Request $request){
		$publicacion = new Publicacion();
		$publicacion->titulo = $request->titulo;
		$publicacion->descripcion = $request->descripcion;
		$publicacion->mascota_id = $request->mascota_id;		
		$publicacion->save();
	}

#Listar publicacions por consulta
/*	public function getPublicacion(Request $request, $id){
		$publicaciones = DB::select("SELECT * FROM publicacion WHERE id = .$id");
		return response()->json($publicaciones, 200);  		
	}	*/

	public function getPublicacion(Request $request){
		$id = $request->id;
		$publicaciones = DB::select("SELECT * FROM publicacion WHERE id =".$id);
		return response()->json($publicaciones, 200);  		
	}

#Modificar publicacion
   public function modifyPublicacion(Request $request, $id){
		//$publicacion = Publicacion::where('publicacion_id', $id)->first();
		$publicacion = Publicacion::where('id', $id)->first();
		$publicacion->titulo = $request->titulo;
		$publicacion->descripcion = $request->descripcion;
		$publicacion->save();          
   }  

#Eliminar publicacion
   public function deletePublicacion(Request $request, $id){
		//$publicacion = Publicacion::where('publicacion_id', $id)->first();
		$publicacion = Publicacion::where('id', $id)->first();
		$publicacion->delete();          
   }  


}
