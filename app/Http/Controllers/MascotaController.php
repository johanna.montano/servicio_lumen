<?php

namespace App\Http\Controllers;

use App\Mascota;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;



class MascotaController extends BaseController
{

#Listar todas las mascotas
    public function index(Request $request){
   		$mascotas = Mascota::all();
   		return response()->json($mascotas, 200);
	}

#Crear una mascota
	public function createMascota(Request $request){
   	$mascota = new Mascota();
   	$mascota->nombres = $request->nombres;
   	$mascota->dueño = $request->dueño;
   	$mascota->fecha_nacimiento = $request->fecha_nacimiento;
	$mascota->celularDueño = $request->celularDueño;
   	$mascota->correoDueño = $request->correoDueño;		
   	$mascota->save();
	}

}
