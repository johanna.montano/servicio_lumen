<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'mascota'], function($router){
	$router->get('/all','MascotaController@index');
	$router->post('/create','MascotaController@createMascota');
});

$router->group(['prefix'=>'publicacion'], function($router){
	$router->get('/all','PublicacionController@index');
	//$router->get('/get/{consulta}','PublicacionController@getPublicacion');	
	$router->post('/get','PublicacionController@getPublicacion');
	$router->post('/create','PublicacionController@createPublicacion');
	$router->post('/modify/{id}','PublicacionController@modifyPublicacion');
	$router->post('/delete/{id}','PublicacionController@deletePublicacion');
});

$router->group(['prefix'=>'mensaje'], function($router){
	$router->get('/all','MensajeController@index');
	$router->get('/get/{id}','MensajeController@getMensaje');
	$router->post('/create','MensajeController@createMensaje');
	$router->post('/delete/{id}','MensajeController@deletePostulacion');
});


$router->group(['prefix'=>'usuarios'], function($router){
	$router->post('/ingresar','UserController@login');
	$router->post('/cerrar_sesion','UserController@logout');	
});