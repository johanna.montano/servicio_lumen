<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensaje', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mensaje');
            
            $table->integer('mascota_id')->unsigned()->nullable();
            $table->integer('publicacion_id')->unsigned()->nullable();

            /**$table->unsignedBigInteger('mascota_id');

            $table->foreign('mascota_id')->references('id')->on('mascota');


            $table->unsignedBigInteger('publicacion_id');

            $table->foreign('publicacion_id')->references('id')->on('publicacion');

            $table->timestamps();**/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('mensaje');
        
        
    }
}
